#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QToolBar>
#include <QLabel>
#include <QPushButton>
#include <QStatusBar>
#include <QFileDialog>
#include <QDir>
#include <QFileInfo>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QMenu *menuFile, *menuView, *subMenuView;
    QAction *actionMenuFileExit, *actionMenuFileOpenFile, *actionMenuViewZoomIn, *actionMenuViewZoomOut, *actionSubMenuEdit1;
    QToolBar *myToolBar;

    QFileDialog *loadImage;
    QDir *listFilesOnDirectory;

    QWidget *mainWidget;
    QVBoxLayout *imageVlayout;
    QHBoxLayout *buttonHlayout;

    QLabel *showImage;
    QPushButton *bttPrev, *bttNext, *bttExit;

    QStringList listFilesOnCurrentDirectory, currentSelectedFile;
    QString myAbsolutePatch, selectedFile;
    QFileInfo currentOpenFile;

    void createMenuFile();
    void createActionMenuFileOpenFile();
    void createActionMenuFileExit();

    void createMenuView();
    void createActionMenuView();
    void createActionMenuZoomIn();
    void createActionMenuZoomOut();

    void createToolBar();

    QList<QString> list;

private slots:
    void openImageFile(const QString &selectedPath);
//    int numberOfItems(int items);
    void nextImage();
    void nextImageSelected(int);
    void previousImage();

signals:
    void num(int);
    void prevNumber(int);
};
#endif // MAINWINDOW_H
