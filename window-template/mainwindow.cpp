#include "mainwindow.h"
#include <QPicture>
#include <QPixmap>
#include <QImage>
quint32 count=0;
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    QSize windowSize;
    windowSize.setHeight(600);
    windowSize.setWidth(800);

    this->resize(windowSize);

    mainWidget = new QWidget();

    imageVlayout = new QVBoxLayout(mainWidget);
    buttonHlayout = new QHBoxLayout();

    showImage = new QLabel(this);
    showImage->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    listFilesOnDirectory = new QDir(QString());

    bttPrev = new QPushButton(this);
    bttPrev->setText(QString(tr("Poprzedni obraz")));

    bttNext = new QPushButton(this);
    bttNext->setText(QString(tr("Następny obraz")));

    bttExit = new QPushButton(this);
    bttExit->setText(QString(tr("Zamknij")));

    imageVlayout->addWidget(showImage);
    imageVlayout->addLayout(buttonHlayout);
    buttonHlayout->addWidget(bttPrev);
    buttonHlayout->addWidget(bttNext);
    buttonHlayout->addWidget(bttExit);

    this->setCentralWidget(mainWidget);

    createActionMenuFileOpenFile();
    createActionMenuFileExit();
    createMenuFile();

    createActionMenuZoomIn();
    createActionMenuZoomOut();
    createMenuView();

    createToolBar();

    QObject::connect(bttNext, &QPushButton::clicked, this, &MainWindow::nextImage);
    QObject::connect(this, &MainWindow::num, this, &MainWindow::nextImageSelected);
    QObject::connect(bttPrev, &QPushButton::clicked, this, &MainWindow::previousImage);
    QObject::connect(bttExit, &QPushButton::clicked, this, &MainWindow::close);
}

void MainWindow::createActionMenuFileOpenFile()
{
    actionMenuFileOpenFile = new QAction(this);
    actionMenuFileOpenFile->setText(QString(tr("Otwórz plik...")));

    loadImage = new QFileDialog(this, Qt::Dialog);
    loadImage->setFileMode(QFileDialog::ExistingFile);
    loadImage->setLabelText(QFileDialog::LookIn, QString(tr("Source is in...")));
    loadImage->setDirectory(QDir::home());
    loadImage->setNameFilters(QStringList{"All files (*.*)","PNG files (*.png)","BMP files (*.bmp)","JPG files (*jpg)"});
    loadImage->setFilter(QDir::Files | QDir::AllDirs);

    QObject::connect(actionMenuFileOpenFile, &QAction::triggered, loadImage, &QFileDialog::show);
//    QObject::connect(loadImage, &QFileDialog::directoryEntered, this, &MainWindow::selectAbsolutePath);
    QObject::connect(loadImage, &QFileDialog::fileSelected, this, &MainWindow::openImageFile);
}

void MainWindow::createActionMenuZoomIn()
{
    actionMenuViewZoomIn = new QAction(this);
    actionMenuViewZoomIn->setText(QString(tr("Powiększ")));
    actionMenuViewZoomIn->setIcon(QIcon(":icons/zoom-in.png"));
}

void MainWindow::createActionMenuZoomOut()
{
    actionMenuViewZoomOut = new QAction(this);
    actionMenuViewZoomOut->setText(QString(tr("Pomniejsz")));
    actionMenuViewZoomOut->setIcon(QIcon(":icons/zoom-out.png"));
}

void MainWindow::createActionMenuFileExit()
{
    actionMenuFileExit = new QAction(this);
    actionMenuFileExit->setText(QString(tr("Zamknij")));

    QObject::connect(actionMenuFileExit, &QAction::triggered, this, &MainWindow::close);
}

void MainWindow::createMenuView()
{
    menuView = new QMenu(this);
    menuView->setTitle(QString(tr("Widok")));

    this->menuBar()->addMenu(menuView);

    menuView->addAction(actionMenuViewZoomIn);
}

void MainWindow::createMenuFile()
{
    menuFile = new QMenu(this);
    menuFile->setTitle(QString(tr("Plik")));

    this->menuBar()->addMenu(menuFile);

    menuFile->addAction(actionMenuFileOpenFile);
    menuFile->addAction(actionMenuFileExit);
}

void MainWindow::createToolBar()
{
    myToolBar = new QToolBar(this);
    myToolBar->addAction(actionMenuFileOpenFile);
    myToolBar->addAction(actionMenuViewZoomIn);
    myToolBar->addAction(actionMenuViewZoomOut);
    myToolBar->addAction(actionMenuFileExit);

    this->addToolBar(myToolBar);
}

void MainWindow::openImageFile(const QString &selectedPath)
{
    list.append("one");
    list.append("two");
    list.append("three");
    list.append("four");
    list.append("five");
}

void MainWindow::nextImage()
{
    quint32 i=1;
    emit num(i);
}

void MainWindow::nextImageSelected(int n)
{
    quint32 i=0;
    count = count + n;
    i = list.size() + count;
    qInfo()<< i << list.value(i);
}

void MainWindow::previousImage()
{
    qint32 i=-1;
    emit num(i);
}

MainWindow::~MainWindow()
{
}

