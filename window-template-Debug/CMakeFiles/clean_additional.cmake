# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles/window-template_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/window-template_autogen.dir/ParseCache.txt"
  "window-template_autogen"
  )
endif()
